<?php

namespace App\Http\Controllers;

use Sunra\PhpSimple\HtmlDomParser;
use HTMLMin\HTMLMin\HTMLMin;
use HTMLMin\HTMLMin\Minifiers\BladeMinifier;
use HTMLMin\HTMLMin\Minifiers\CssMinifier;
use Browser\Casper;
use HTMLMin\HTMLMin\Minifiers\HtmlMinifier;
use HTMLMin\HTMLMin\Minifiers\JsMinifier;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Response;
use Cookie;
use Illuminate\Http\Request;

class Controller extends BaseController {

	use AuthorizesRequests,
	DispatchesJobs,
	ValidatesRequests;

	public function __construct() {
		View::share('money', Config::get('money'));
		$this->filters = Config::get('filters');
		$this->lines = \file(Storage::disk('local')->url('keywords.txt'), \FILE_IGNORE_NEW_LINES | \FILE_SKIP_EMPTY_LINES);
		$this->keys = [];
		foreach ($this->lines as $l) {
			$this->keys = array_add($this->keys, str_replace('.', '', $l), str_slug($l));
		}
	}

	public function change() {
		$a = json_decode(file_get_contents("http://pool.wis.nu/wallpaper/keyword"));
		foreach ($a as $a) {
			$a = str_replace('keywords/', '', $a);
			echo '<li><a href="change/' . $a . '">' . $a . '</a></li>';
		}
	}

	/*     *
     * 	Get homepage using keywords.txt file
     *
     */

	public function index() {
		$lines = file(Storage::disk('local')->url('keywords.txt'), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		shuffle($lines);
		$result = array_slice($lines, 0, 1000);

		if (Cache::has('index_related')) {
			$related = Cache::get('index_related');
		} else {
			$related = Cache::remember('index_related', 100, function () use ($lines) {
					shuffle($lines);
					return array_slice($lines, 1000, 42);
				});
		}



		return view('index')
		->with('result', $result)
		->with('related', $related)
		;
	}

	/*     *
     * 	Render category page
     *
     * 	@param string $slug
     */

	public function single($slug, $page = 1) {
		$sanitizeBadwords = str_replace('-', ' ', $slug);

		if ($this->contains(explode(',', $this->filters['badwords']), $sanitizeBadwords)) {
			return abort(404);
		} else {
			if ($keyword = array_keys($this->keys, $slug, false)) {   // check slug exist in keywords.txt
				shuffle($this->lines);
				$cachekey = md5($keyword[0]);
				if (Cache::has($cachekey . '.json')) {
					$result = Cache::get($cachekey . '.json');
				} else {
					$result = $this->getJson('http://pool.wis.nu/api/wsearch/' . urlencode($keyword[0]));
					//      $result = file_get_contents('http://'.  file_get_contents('https://api.ipify.org').'/scrape/' . urlencode($keyword[0]));
					Cache::put($cachekey . '.json', $result, 1440);
					$result = Cache::get($cachekey . '.json');
				}

				$related = array_slice($this->lines, 1000, 63);
				$related2 = array_slice($this->lines, 0, 90);
			} else {
				return abort(500);
			}
		}

		return view('single')
		->with('page', $page)
		->with('title', ucwords(str_replace('-', ' ', $slug)))
		->with('result', $result)
		->with('related', $related)
		->with('related2', $related2)
		// ->render()
		;
	}

	/*     *
     * 	Render single page
     *
     * 	@param string $slug
     * 	@param string $permalink (free to change)
     * 	@param string $id 	// base64_encode with substr (__,0,18)
     */

	public function attachment($slug, $permalink) {
		$sanitizeBadwords = str_replace('-', ' ', $slug);

		if ($this->contains(explode(',', $this->filters['badwords']), $sanitizeBadwords)) {
			return abort(404);
		} else {
			if ($keyword = array_keys($this->keys, $slug, false)) {   // check slug exist in keywords.txt
				$cachekey = md5($keyword[0]);
				if (Cache::has($cachekey . '.json')) {
					$result = Cache::get($cachekey . '.json');
				} else {
					$result = $this->getJson('http://pool.wis.nu/api/wsearch/' . urlencode($keyword[0]));
					//      $result = file_get_contents('http://67.205.191.36/scrape/' . urlencode($keyword[0]));
					Cache::put($cachekey . '.json', $result, 1440);
					$result = Cache::get($cachekey . '.json');
				}


				$resultArr = json_decode($result, true);
				$result = json_decode($result);

				$valNum = array_search($permalink, array_column($resultArr['data'], 'slug'));

				$related = array_slice($this->lines, 1000, 63);
				$related2 = array_slice($this->lines, 0, 90);
			} else {
				return abort(404);
			}
		}
		return view('attachment')
		->with('id', $valNum)
		->with('slug', $slug)
		->with('result', $result)
		->with('resultArr', $resultArr)
		->with('related', $related)
		->with('related2', $related2)
		->with('suggestion', @implode(' ', $this->suggestions(str_replace('-', ' ', $slug))))
		->render()
		;
	}

	/*     *
     * 	Static page.
     *
     * 	@param string $slug

     */

	public function page($slug) {
		if ($slug == 'privacy-policy') {
			$pageName = 'Privacy Policy';
		} elseif ($slug == 'dmca') {
			$pageName = 'DMCA';
		} elseif ($slug == 'toc') {
			$pageName = 'Terms of Conditions';
		} elseif ($slug == 'contact') {
			$pageName = 'Contact Us';
		}

		return view('page')
		->with('page', $pageName)
		->with('content', Config::get("themes.$slug"))
		//  ->with('related', array_slice($this->data, 0, 10))
		->render();
	}

	public function contains($needles, $haystack) {
		return count(array_intersect($needles, explode(" ", preg_replace("/[^A-Za-z0-9' -]/", "", $haystack))));
	}

	/*     *
     * 	Helper
     *
     */

	public function suggestions($slug) {
		$relatedGo = json_decode(@file_get_contents("http://suggestqueries.google.com/complete/search?client=firefox&q=$slug&hl=en"));
		if ($relatedGo === FALSE) {
			$related = '';
			return $related;
		} elseif ($relatedGo === TRUE) {
			$related = array();
			foreach ($relatedGo[1] as $r1) {
				$related[] = ucfirst($r1);
			}

			return $related;
		}
	}

	public function scrape($keyword) {
		if (Cache::has(md5(urldecode($keyword)))) {
			return Cache::get(md5(urldecode($keyword)));
		} else {
			$result = json_encode($this->casper($keyword));

			Cache::put(md5(urldecode($keyword)), $result, 600);
			return Cache::get(md5(urldecode($keyword)));
		}
	}

	/*
     * 	Casper
     *
     * 	@param	string	$keyword
     */

	public function casper($keyword) {
		$casper = new Casper();
		$casper->setOptions([
			'ignore-ssl-errors' => 'yes'
			]);

		$tmpFile = storage_path('app/wallpaper/' . md5(urldecode($keyword)) . '.json');
		// navigate to google web page
		$casper->start('https://www.google.com/search?&q=' . str_replace("'", "", urldecode($keyword)) . '&start=0&asearch=ichunk&tbm=isch&tbs=isz:m', $tmpFile);
		$casper->run();
		$html = json_decode(file_get_contents($tmpFile));
		$html = $html[1][1];
		$html = HtmlDomParser::str_get_html($html);

		$json['keyword'] = urldecode($keyword);
		$json['data'] = array();
		foreach ($html->find('div.rg_meta') as $a) {
			$a = json_decode($a->innertext);
			$data['gid'] = $a->id;
			$data['visibleUrl'] = $a->isu;
			$data['height'] = (int) $a->oh;
			$data['width'] = (int) $a->ow;
			$data['url'] = $a->ou;
			$data['content'] = isset($a->pt) ? $a->pt : NULL;
			$data['slug'] = str_slug($a->pt . ' ' . hash('crc32', $a->id, FALSE));
			$data['originalContextUrl'] = $a->ru;
			$data['title'] = isset($a->st) ? $a->st : NULL;
			$data['tbWidth'] = (int) $a->tw;
			$data['tbHeight'] = (int) $a->th;
			$data['tbUrl'] = $a->tu;
			//    $data['notes'] = 'inserted';

			$json['data'][] = $data;
		}
		unlink($tmpFile);
		return $json;
	}


	public function getJson($url)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Accept: application/json",
					"Authorization: Bearer ".Config::get('money.token'),
					"Cache-Control: no-cache",
				),
			));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		return $response;
	}

}
