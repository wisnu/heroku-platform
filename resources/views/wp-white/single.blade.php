  @php
    $arr = json_decode($result);
    $arra = json_decode($result, true);
  @endphp
@extends('main')
@section('title')
  [{{ $title }}] 

  @for ($i=0; $i<3; $i++)
  {{ ucwords(str_replace('-', ' ', str_slug($arra['data'][$i]['content']))) }},
  @endfor
@endsection

@section('meta')
<meta name="keywords" content="{{ ucwords(str_replace('-', ' ', $title)) }}, {{ implode(', ', $related) }}">
<meta name="description" content="{{ ucwords($title) }} {{ implode(' ', $related) }}">


@endsection

@section('content')
  <div id='dd'>
    <div class="crumbs">
      <span typeof="v:Breadcrumb"><a href="/" property="v:title" rel="v:url">Home</a></span> &raquo; <span class='crent' typeof="v:Breadcrumb">{{ ucwords($title) }}</span>
    </div>
    <div id="dl">
      <div class="content">
        <h1 class='ld'>{{ $title }}</h1>
        <div class='ads-top'>
          <?=$money['responsiveAds']; //Ads ?>
        </div>
        <p>Showing {{ (($page - 1) * count($arr->data) ) + 1 }} - {{ $page * count($arr->data) }} of {{ rand(1300,2700) }} results from {{ $title }} images</p>
        @foreach ($arr->data as $result)
    
        <div class="box">
          <a class='th' href="{{ url('/'.str_slug($title).'/'.$result->slug) }}.html" title="{{ $result->content }}"><img alt="{{ $result->content }}" height="100" onerror="null;this.src='{{ $result->tbUrl }}'" src="{{ $result->url }}" width="225"></a>
          <h2>{{ $result->content }}</h2>
        </div>

        @endforeach

        <div style="clear:both"></div>
        <div style="clear:both"></div>
        <h3 class='ld'>Related {{ $title }}</h3>
        <ul>
          @foreach ($related2 as $rel2)
            <li><a href="{{ url(str_slug(ucwords($rel2))) }}" title="{{ ucwords(ucwords($rel2)) }}">{{ ucwords(ucwords($rel2)) }}</a></li>
          @endforeach
        </ul>
      </div>
    </div>
    <div id="sb">
      <div class="dl"></div>
      <div class="ads-right">
        <?=$money['responsiveAds']; //Ads ?>
      </div>
      <h3 class="hc">Random post:</h3>
      <ul class="rand-text">
        @foreach ($related as $rel)
          <li><h3><a href="{{ url(str_slug(ucwords($rel))) }}" title="{{ ucwords(ucwords($rel)) }}">{{ ucwords(ucwords($rel)) }}</a></h3></li>
        @endforeach
      </ul>
      <div class="ads-right">
        <?=$money['responsiveAds']; //Ads ?>
      </div>
      <div class="dl"></div>
    </div>
    <div class="dl"></div>
  </div>
@endsection
