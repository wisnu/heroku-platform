@extends('main')
@section('title')
	{{ ucwords(str_replace('-', ' ', $page)) }}
@endsection

@section('content')
	<div id='dd'>
		<div class="crumbs">
			<span typeof="v:Breadcrumb"><a href="/" property="v:title" rel="v:url">Home</a></span>
		</div>
		<div id="dl">
			<div class="content">
				<h1>{{ $page }}</h1>
				<p><?=$content;?></p>
			</div>
		</div>
		<div id="sb">
			<div class="dl"></div>
			<h3 class="hc">Random post:</h3>
			<ul class="rand-text"></ul>
			<div class="dl"></div>
		</div>
		<div class="dl"></div>
	</div>
@endsection
