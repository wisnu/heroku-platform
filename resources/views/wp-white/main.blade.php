<!DOCTYPE html>
<html>
<head>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<title>
		@yield('title')
	</title>
	<link href='{{ url()->current() }}' rel='canonical'>
	<meta content="index,follow,imageindex" name="googlebot">
	<meta content="all,index,follow" name="robots">
	<meta content="all,index,follow" name="googlebot-Image">
	<meta content="width=device-width,initial-scale=1" name="viewport">
	<meta content="general" name="rating">
	<meta content="global" name="distribution">
		@yield('meta')
	<meta content="en" name="language">
	<link href="{{ theme_url('favicon.ico') }}" rel="shortcut icon" type="image/x-icon">
	<link href="{{ theme_url('icon.png') }}" rel="apple-touch-icon">
	<link href="{{ theme_url('icon.png') }}" rel="icon" sizes="57x57">
	<meta content="#FF6600" name="msapplication-TileColor">
	<meta content="{{ url()->current() }}" name="application-name">
	<meta content="{{ url()->current() }}" name="msapplication-tooltip">
	<meta content="{{ theme_url('icon.png') }}" name="msapplication-TileImage" title="favicon ie10">
	<link href="{{ theme_url('style.css') }}" id="simpress_main_style-css" rel="stylesheet">
</head>
<body>
	<div id="kp">
		<div class="kr">
			{{ $_SERVER['HTTP_HOST'] }}
		</div>
		<div class="kn">
			<ul class='menu'>
				<li>
					<a href="{{ url('page/contact.html') }}" rel="nofollow">Contact</a>
				</li>
				<li>
					<a href="{{ url('page/dmca.html') }}" rel="nofollow">DMCA</a>
				</li>
				<li>
					<a href="{{ url('page/toc.html') }}" rel="nofollow">Terms</a>
				</li>
				<li>
					<a href="{{ url('page/privacy-policy.html') }}" rel="nofollow">Privacy Policy</a>
				</li>
			</ul>
		</div>
		<div class="cl"></div>
	</div>
	
	@yield('content')
	
	
	<div class="bf">
		Copyright © 2017 {{ $_SERVER['HTTP_HOST'] }}.
	</div>
   <!-- Histats.com  START  (aync)-->
<script>
if(!Histats_variables){var Histats_variables=[];}
Histats_variables.push("{{ $_SERVER['HTTP_HOST'] }}");
</script>
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,{{ $money['trackid'] }},4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?{{ $money['trackid'] }}&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
<?php
	if ($money['ga'] == true) {
	echo '<!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id='.$money['gaid'].'"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag(\'js\', new Date());

  gtag(\'config\', \''.$money['gaid'].'\');
</script>
';
	}
?>


</body>
</html>


