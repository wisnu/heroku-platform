@extends('main')
@section('title')
	Free Download {{ ucwords(str_replace('-', ' ', $slug)) }}
@endsection

@section('meta')
<meta name="description" content="Download {{ ucwords($item->data[0]->content) }}">
<meta name="keywords" content="{{ implode(', ', array_slice($related, 0, 10)) }}">

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Article",
  "image": "{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}",
  "name": "{{ ucwords(str_replace('-', ' ', $slug)) }} {{ ucwords($item->data[0]->content) }}",
  "publisher": "{{ $_SERVER['HTTP_HOST'] }}",
  "author": "{{ $_SERVER['HTTP_HOST'] }}",
  "headline": "{{ ucwords($item->data[0]->content) }}",
  "datePublished": "2017-05-31"
}
</script>
@endsection

@section('content')
<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div id="main-container" class="col-md-9">
							<ol class="breadcrumb">
								<li><a href="{{ route('home') }}">Home</a></li>
								<li><a href="{{ url($slug) }}">{{ ucwords(str_replace('-', ' ', $slug)) }}</a></li>
								<li class="active">{{ ucwords($item->data[0]->content) }}</li>
							</ol><!--breadcrumb-->

							<div>
								<?=$money['responsiveAds']; //Ads ?>	
							</div>
							<article class="post-content">
								<div class="single-article-header">
									<img src="{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}" class="img-responsive" alt="single" id="exifviewer-img-5"  width="100%" oldsrc="{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}">
								</div><!--post-header-->
								<h2 class="single-article-title">{{ ucwords($item->data[0]->content) }}</h2><!--post-title-->

								<div class="post-entry push_bottom_30">
									<p></p>

								</div><!--post-entry-->


					        </article><!--post-content-->

							<section class="category-box related-posts scroll-box push_bottom_30">

								<div class="scroll-content" id="scroll-1">
									<div class="scroll-item row">
										@foreach ($result->data as $data)
										<?php
$tbId = explode('tbn:', $data->tbUrl);
$tbId = $tbId[1];

$imgId = explode('.', explode('/', $data->url)[4]);
$imgId = $imgId[0];

?>

										<article class="scroll-article col-md-4 col-sm-4">
											<div class="article-image">
												<a href="{{  str_slug($data->content).'_'.$imgId }}.html" rel="bookmark">
													<h3 class="article-title">{{ $data->content }}</h3>
													<img src="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/262/140') }}" alt="{{ $data->content }}" id="exifviewer-img-7" oldsrc="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/262/140') }}">
												</a>
											</div>
										</article>
										@endforeach

									</div><!--scrool-item-->
								</div><!--scroll-content-->
							</section><!--related-box-->

						</div>
						<aside class="sidebar col-md-3">
							<div class="widget widget_search push_bottom_30">
								<form role="search" method="get" action="archive.html" class="search-form">
									<div class="form-group">
										<input type="text" name="s" value="Type a keyword and hit enter ....." onfocus="if (this.value == 'Type a keyword and hit enter .....') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Type a keyword and hit enter .....';}" class="form-control search-widget-input">
									</div><!-- from group -->
								</form><!-- search form -->
							</div><!-- Search widget-->

							<div class="widget widget-tabbed push_bottom_30">
							<?=$money['responsiveAds']; //Ads ?>	
							</div>

							<div class="widget widget-tabbed push_bottom_30" id="widget_tabs">
								<div class="panel-group">
									<div class="tab-content">
										<div class="tab-pane box-content row active" id="recent_widget_tabs">
											<article class="article other-article side-article col-md-12">
												@foreach ($related as $rel)
												<h4 class="article-title"><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}" rel="bookmark">{{ ucwords($rel) }}</a></h4>
												@endforeach

											</article>
										</div>

@endsection